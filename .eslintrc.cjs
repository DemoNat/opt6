/* eslint-env node */
module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    'plugin:vue/vue3-strongly-recommended',
    'eslint:recommended',
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {
    'vue/singleline-html-element-content-newline': ['error', {
      'ignoreWhenNoAttributes': false,
      'ignoreWhenEmpty': false,
      'ignores': ['textarea',],
    }],
    'vue/block-tag-newline': ['error', {
      'singleline': 'always',
      'multiline': 'always',
      'maxEmptyLines': 0,
      'blocks': {
        'template': {
          'singleline': 'always',
          'multiline': 'always',
          'maxEmptyLines': 0,
        },
        'script': {
          'singleline': 'always',
          'multiline': 'always',
          'maxEmptyLines': 0,
        },
        'my-block': {
          'singleline': 'always',
          'multiline': 'always',
          'maxEmptyLines': 0,
        }
      }
    }]
    
  }
}
